package com.alfredo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alfredo.model.Fund;
import com.alfredo.service.FundService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class FundController {
	private static final Logger LOG = LoggerFactory.getLogger(FundController.class);
	
	@Autowired
	FundService fundService;
	
	@PostMapping("/fund")
	public Fund create(@RequestBody Fund fund) {
		LOG.debug("Received fund create request for[{}]", fund);//log
		return fundService.create(fund);//create fund
	}

	@GetMapping("/fund/investor/{investorId}")
	public List<Fund> getFundByInvestorId(@PathVariable("investorId") int id){
		LOG.debug("Received get funds by investor request for investor with id [{}]", id);//log
		
		return fundService.getFundByInvestorId(id);//get funds by Investor id
	}
	
	@DeleteMapping("/fund/{id}")
	public Fund deleteFund(@PathVariable int id) {
		LOG.debug("Received delete request for id [{}]", id);//log
		return fundService.delete(id);
	}
	
	@PutMapping("/fund/{id}")
	public Fund update(@PathVariable int id, @RequestBody Fund fund) {
		LOG.debug("Received fund create request for[{}]", fund);//log
		
		fund.setId(id);//set the fund id to be updated
		return fundService.update(fund);//create fund
	}
	
	@GetMapping("/fund/{id}")
	public Fund getFund(@PathVariable int id) {
		return fundService.read(id);
	}
	
	@GetMapping("/fund")
	public List<Fund> getAll(){
		return fundService.getAll();
	}
}
