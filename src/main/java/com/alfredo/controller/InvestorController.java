package com.alfredo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alfredo.model.Investor;
import com.alfredo.model.InvestorWrapper;
import com.alfredo.service.FundService;
import com.alfredo.service.InvestorService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class InvestorController {
	private static final Logger LOG = LoggerFactory.getLogger(InvestorController.class);
	
	@Autowired
	InvestorService investorService;
	
	@Autowired
	FundService fundService;
	
	@PostMapping("/investor/")
	public Investor create(@RequestBody InvestorWrapper investorInfo) {
		LOG.debug("Received create investor request for [{}]", investorInfo);//log
		System.out.println("#####################");
		return investorService.create(investorInfo);//call investor service to create investor
	}
	
	@GetMapping("/investor/{id}")//provide investor id
	public Investor read(@PathVariable int id) {
		LOG.debug("Received read investor request for [{}]", id);//log
		
		return investorService.read(id);//call investor service to read investor with id
	}
	
	@PutMapping("/investor/{id}")//provide investor id
	public Investor update(@PathVariable int id, @RequestBody InvestorWrapper investorInfo) {
		LOG.debug("Received update investor request for [{}]",id, investorInfo);//log
		
		investorInfo.getInvestor().setId(id);	//set id of the investor to be updated
		return investorService.update(investorInfo);//call investor service to update data
	}
	
	@GetMapping("/investor/client/{id}")//provide client id
	public List<Investor> getInvestorByClientId(@PathVariable int id){
		LOG.debug("Received get investor for a client request for client id [{}]",id);//log
		return investorService.getInvestorByClientId(id);//get investor for the client with id
	}
	
	@GetMapping("/investor")
	public List<Investor> getAll(){
		LOG.debug("Received get all investors request");//log
		return investorService.getAll();//call investor service to get all investors

	}

}
