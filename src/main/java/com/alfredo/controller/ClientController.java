package com.alfredo.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.alfredo.model.Client;
import com.alfredo.model.ClientWrapper;
import com.alfredo.service.ClientService;
import com.alfredo.service.InvestorService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ClientController {
	private static final Logger LOG = LoggerFactory.getLogger(ClientController.class);
	
	@Autowired
	ClientService clientService;
	@Autowired
	InvestorService investorService;
	
	@PostMapping("/client")
	public Client create(@RequestBody ClientWrapper clientInfo) {//create client
		LOG.debug("Received client create request for [{}] ",clientInfo.getClient());//log
		return clientService.create(clientInfo);//call client service to persist data
	}
	
	@GetMapping("/client/{id}")
	public Client read(@PathVariable int id) {//get client by id
		LOG.debug("Received client read request for id [{}]", id);//log
		return clientService.read(id);//call client service to get the data
	}
	
	@PutMapping("/client/{id}")
	public Client update(@PathVariable int id, @RequestBody ClientWrapper clientInfo) {//update client
		LOG.debug("Received client update request for id [{}]", id);//log
		clientInfo.getClient().setId(id);//set the investor for the client
		return clientService.update(clientInfo);//call client service to update the data and persist
	}
	
	@GetMapping("/client")
	public List<Client> getAll(){//get all clients
		LOG.debug("Received client get all request");//log
		return clientService.getAll(); //call client service to get all clients
	}
}
