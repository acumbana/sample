package com.alfredo.model;

import java.util.List;

public class InvestorWrapper {
	List<Integer> fundId;
	Investor investor;

	public List<Integer> getFundId() {
		return fundId;
	}

	public void setFundId(List<Integer> fundId) {
		this.fundId = fundId;
	}

	public Investor getInvestor() {
		return investor;
	}

	public void setInvestor(Investor investor) {
		this.investor = investor;
	}
	
}
