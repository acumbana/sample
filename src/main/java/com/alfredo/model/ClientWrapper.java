package com.alfredo.model;

import java.util.List;

public class ClientWrapper {
	private List<Integer> investorId;
	private Client client;
	public List<Integer> getInvestorId() {
		return investorId;
	}
	public void setInvestorId(List<Integer> investorId) {
		this.investorId = investorId;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
}
