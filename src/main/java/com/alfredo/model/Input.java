package com.alfredo.model;

public class Input {

	InvestorWrapper id;
	Investor investor;
	public InvestorWrapper getId() {
		return id;
	}
	public void setId(InvestorWrapper id) {
		this.id = id;
	}
	public Investor getInvestor() {
		return investor;
	}
	public void setInvestor(Investor investor) {
		this.investor = investor;
	}
	
}
