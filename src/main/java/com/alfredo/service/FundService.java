package com.alfredo.service;

import java.util.List;

import com.alfredo.model.Fund;

public interface FundService {
	public List<Fund> getAll();
	public Fund create(Fund fund);
	public Fund read(int id);
	public Fund update(Fund fund);
	public List<Fund> getFundByInvestorId(int id);
	public Fund delete(int id);
}
