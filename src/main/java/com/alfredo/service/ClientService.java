package com.alfredo.service;

import java.util.List;

import com.alfredo.model.Client;
import com.alfredo.model.ClientWrapper;

public interface ClientService {
	public List<Client> getAll();
	public Client create(ClientWrapper clientInfo);
	public Client read(int id);
	public Client update(ClientWrapper clientInfo);
}
