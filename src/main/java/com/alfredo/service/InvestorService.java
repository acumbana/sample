package com.alfredo.service;

import java.util.List;

import com.alfredo.model.InvestorWrapper;
import com.alfredo.model.Investor;

public interface InvestorService {
	public List<Investor> getAll();
	public Investor create(InvestorWrapper investorInfo);
	public Investor read(int id);
	public Investor update(InvestorWrapper investorInfo);
	public List<Investor> getInvestorByClientId(int id);
}


