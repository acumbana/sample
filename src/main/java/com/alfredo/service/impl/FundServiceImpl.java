package com.alfredo.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alfredo.dao.FundRepository;
import com.alfredo.model.Fund;
import com.alfredo.service.FundService;

@Service
public class FundServiceImpl implements FundService {
	private static final Logger LOG = LoggerFactory.getLogger(FundServiceImpl.class);

	@Autowired
	FundRepository fundRepo;
	@Override
	public List<Fund> getAll() {//get all funds
		LOG.debug("getting all funds"); //logger
		return fundRepo.findAll(); 
	}

	@Override
	public Fund create(Fund fund) {
		LOG.debug("Creating fund [{}]",fund);//logger

		if(fund.getName()==null||fund.getName().equals("")) {
			throw new RuntimeException("Fund name cannot be empty");
		}
		
		return fundRepo.save(fund); //save fund
	}

	@Override
	public Fund read(int id) {
		LOG.debug("Geeting fund with id [{}]",id);//logger
		return fundRepo.findById(id).orElse(null);//get fund else return null
	}

	@Override
	public Fund update(Fund fund) {
		LOG.debug("Updating fund with id [{}]",fund.getId());//logger
		fundRepo.findById(fund.getId()).orElseThrow(()-> new RuntimeException("Invalid fund id: "+fund.getId())); //check if the fund with this id exist else throw exception
		return fundRepo.save(fund);//update
	}

	@Override
	public List<Fund> getFundByInvestorId(int id) {
		LOG.debug("Getting fund for investor with id [{}]",id);//log
		return fundRepo.getFundByInvestorId(id);//get fund list of an investor
	}

	@Override
	public Fund delete(int id) {
		LOG.debug("Deleting fund with id [{}]",id);
		Fund fund = fundRepo.findById(id).orElse(null);
		if(fund!=null)	//if fund with this id exist delete
			fundRepo.delete(fund);
		return fund;
	}

}
