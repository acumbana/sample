package com.alfredo.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alfredo.dao.ClientRepository;
import com.alfredo.dao.InvestorRepository;
import com.alfredo.model.Client;
import com.alfredo.model.ClientWrapper;
import com.alfredo.model.Investor;
import com.alfredo.service.ClientService;
import com.alfredo.service.InvestorService;

@Service
public class ClientServiceImpl implements ClientService {
	
	private static final Logger LOG = LoggerFactory.getLogger(ClientServiceImpl.class);
	
	@Autowired
	ClientRepository clientRepo;
	
	@Autowired
	InvestorRepository investorRepo;
	
	@Autowired
	InvestorService investorService;
	

	@Override
	public Client create(ClientWrapper clientInfo) {	// create a new client
		LOG.debug("Creating client [{}]",clientInfo.getClient());//log
		
		Client client = clientInfo.getClient();
		Client clientDB = clientRepo.findByName(client.getName());//check if a client with this name exist
		
		if(clientInfo.getClient().getName()==null ||clientInfo.getClient().getName().equals("")) {//check if the name field is empty
			throw new RuntimeException("Client name field cannot be empty");
		}
		if(clientDB!=null) {//if client with the same name exist throw exception
			throw new RuntimeException("Client with this name already exist with id "+clientDB.getId());
		}
		clientDB = clientRepo.findById(client.getId()).orElse(null);
		if(clientDB!=null) {
			throw new RuntimeException("Client with id "+ clientDB.getId()+" already existed");
		}
		
		List<Integer> investorId = clientInfo.getInvestorId();
		Set<Investor> investors = new HashSet<>();
		
		for(int id:investorId) {
			Investor investor = investorRepo.findById(id).orElseThrow(()->new RuntimeException("Invalid Investor Id: "+id));
			if(investor.getClient()!=null) {
				throw new RuntimeException("Investor with id "+investor.getId()+" already has client");
			}
			investor.setClient(client);
			investors.add(investor);
		}
		
		client.setInvestor(investors);
		return clientRepo.save(client);
	}

	@Override
	public Client read(int id) { // get a client else throw an exception with message invalid client id number
		LOG.debug("Getting client with id [{}]",id);//log
		return clientRepo.findById(id).orElse(null);
	}

	@Override
	public Client update(ClientWrapper clientInfo) {//update client if exist otherwise throw an exception with invalid client id message
		LOG.debug("Updating client with id [{}]",clientInfo.getClient());//log
		
		Client client = clientInfo.getClient();
		Client clientDB = clientRepo.findById(client.getId()).orElse(null);
		
		if(clientDB==null) {
			throw new RuntimeException("Invalid client id: "+client.getId());
		}
		
		List<Integer> investorId = clientInfo.getInvestorId();
		Set<Investor> investors = new HashSet<>();
		
		for(int id:investorId) {
			Investor investor = investorRepo.findById(id).orElseThrow(()->new RuntimeException("Invalid investor Id: "+id));
			investor.setClient(client);
			investors.add(investor);
		}
		
		client.setInvestor(investors);
		return clientRepo.save(client);
	}
	
	
	
	@Override
	public List<Client> getAll() {//get all clients for DB
		LOG.debug("Getting all clients");//log
		return clientRepo.findAll();
	}


}
