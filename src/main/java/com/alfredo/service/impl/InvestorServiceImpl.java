package com.alfredo.service.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alfredo.dao.FundRepository;
import com.alfredo.dao.InvestorRepository;
import com.alfredo.model.Fund;
import com.alfredo.model.InvestorWrapper;
import com.alfredo.model.Investor;
import com.alfredo.service.FundService;
import com.alfredo.service.InvestorService;

@Service
@Transactional
public class InvestorServiceImpl implements InvestorService {

	private static final Logger LOG = LoggerFactory.getLogger(InvestorServiceImpl.class);
	
	@Autowired
	InvestorRepository investorRepo;
	
	@Autowired
	FundService fundService;
	
	@Autowired
	FundRepository fundRepo;
	
	@Override
	public List<Investor> getAll() {
		LOG.debug("Getting all Investors");//log
		return investorRepo.findAll();//getting all investors
	}

	@Override
	public Investor create(InvestorWrapper investorInfo) {
		LOG.debug("Creating investor [{}] ",investorInfo.getInvestor());//log
		Investor investor = investorInfo.getInvestor();
		
		if(investorInfo.getInvestor().getName()==null || investorInfo.getInvestor().getName().equals("")) {//check if the name field is empty
			throw new RuntimeException("Investor name field cannot be empty");
			
		}
		
		List<Integer> listOfFunds = investorInfo.getFundId();//List of primary keys of fund
		Set<Fund> funds = new HashSet<>();//creating a new Hashset
		
		for(int id:listOfFunds) {//checking if funds with these id's exist in DB and add them to the list
			Fund fund = fundRepo.findById(id).orElseThrow(()-> new RuntimeException("Invalid fund id: "+id));
			if(fund.getInvestor()!=null) {//one fund can only have one investor(owner)
				throw new RuntimeException("Fund with id "+id+" already has ower with id "+fund.getInvestor().getId());
			}
			fund.setInvestor(investor);
			funds.add(fund);	//creating a set of funds based on the given id's
		}
		
		investor.setFund(funds);
		return investorRepo.save(investorInfo.getInvestor());//save investor
	}

	@Override
	public Investor read(int id) {//get investor with id
		LOG.debug("Getting investor with id [{}] ",id);//log
		return investorRepo.findById(id).orElse(null);
	}

	@Override
	public Investor update(InvestorWrapper investorInfo) {
		LOG.debug("Updating investor with id: "+investorInfo.getInvestor().getId());
		
		Investor investor = investorInfo.getInvestor();//get the investor from the info provided
		Investor investorDB = investorRepo.findById(investor.getId()).orElse(null);//get investor with the provided id(if provided)
		
		if(investorDB==null) {
			throw new RuntimeException("Investor with id "+investor.getId()+" does not exist");
		}
		
		List<Integer> listOfFunds = investorInfo.getFundId();//List of primary keys of fund
		Set<Fund> funds = new HashSet<>();//creating a new Hashset
		
		for(int id:listOfFunds) {//checking if funds with these id's exist in DB and add them to the list
			Fund fund = fundRepo.findById(id).orElseThrow(()-> new RuntimeException("Invalid fund id: "+id));
			fund.setInvestor(investor);
			funds.add(fund);	//creating a set of funds based on the given id's
		}
		
		investor.setFund(funds);
		return investorRepo.save(investor);
	}

	@Override
	public List<Investor> getInvestorByClientId(int id) {
		LOG.debug("Getting investors for client of id [{}] ",id);//log
		return investorRepo.getInvestorByClientId(id);//get investor for a client with a particular id
	}

	
	
	
	
	
}
