package com.alfredo.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.alfredo.model.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer>{
	public Client findByName(String name);
	
}
