package com.alfredo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.alfredo.model.Fund;

@Repository
public interface FundRepository extends JpaRepository<Fund, Integer> {

	@Query("SELECT f FROM fund AS f WHERE f.investor.id = :id")//custom query(JPQL) to get fund by investor id
	public List<Fund> getFundByInvestorId(@Param("id")int id);
	public Fund findByName(String name);
}
