package com.alfredo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.alfredo.model.Investor;

@Repository
public interface InvestorRepository extends JpaRepository<Investor, Integer> {
	
	//creating a custom query (JPQL) to get investors for a particular client using client's id
	@Query("SELECT i FROM investor AS i WHERE i.client.id = :id")
	public List<Investor> getInvestorByClientId(@Param("id")int id);
	
	public Investor findByName(String name);
} 
